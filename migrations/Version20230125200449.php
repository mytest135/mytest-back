<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230125200449 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added isDeleted property to Book, Dvd and furniture';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book ADD is_deleted TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE dvd ADD is_deleted TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE furniture ADD is_deleted TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book DROP is_deleted');
        $this->addSql('ALTER TABLE dvd DROP is_deleted');
        $this->addSql('ALTER TABLE furniture DROP is_deleted');
    }
}
