<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Repository\BookRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BookRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['book:write']],
    normalizationContext: ['groups' => ['book:read', 'books:read']],
)]
class Book implements IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['books:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['books:read', 'book:write'])]
    private $sku;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['books:read', 'book:write'])]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['books:read', 'book:write'])]
    private $price;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['books:read', 'book:write'])]
    private $weight;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['books:read', 'book:write'])]
    private $isDeleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }
}
