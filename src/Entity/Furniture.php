<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Repository\FurnitureRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FurnitureRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['furniture:write']],
    normalizationContext: ['groups' => ['furniture:read', 'furnitures:read']],
)]
class Furniture implements IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['furnitures:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['furnitures:read', 'furniture:write'])]
    private $sku;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['furnitures:read', 'furniture:write'])]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['furnitures:read', 'furniture:write'])]
    private $price;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['furnitures:read', 'furniture:write'])]
    private $height;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['furnitures:read', 'furniture:write'])]
    private $width;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['furnitures:read', 'furniture:write'])]
    private $length;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['furnitures:read', 'furniture:write'])]
    private $isDeleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getLength(): ?string
    {
        return $this->length;
    }

    public function setLength(string $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }
}
