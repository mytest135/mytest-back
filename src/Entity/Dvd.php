<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Repository\DvdRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: DvdRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['dvd:write']],
    normalizationContext: ['groups' => ['dvd:read', 'dvds:read']],
)]
class Dvd implements IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['dvds:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['dvds:read', 'dvd:write'])]
    private $sku;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['dvds:read', 'dvd:write'])]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['dvds:read', 'dvd:write'])]
    private $price;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['dvds:read', 'dvd:write'])]
    private $size;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['dvds:read', 'dvd:write'])]
    private $isDeleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }
}
